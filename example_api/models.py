# -*- encoding: utf-8 -*-
from django.db import models
from taggit.managers import TaggableManager

from base.model_utils import TimedCreateModifyDeleteModel


class DeleteableMe(TimedCreateModifyDeleteModel):
    """Test the 'api.api_utils.SoftDeleteViewSet' helper."""

    name = models.TextField()


class ExampleModelTag(TimedCreateModifyDeleteModel):
    """Test 'api.serializers.ModelTagSerializer'."""

    name = models.TextField()
    tags = TaggableManager()
    # blank=True, through=SkillTag, related_name="skill_tags"
    # )
