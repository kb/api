# -*- encoding: utf-8 -*-
from rest_framework import serializers

from api.serializers import ModelTagSerializer
from .models import DeleteableMe, ExampleModelTag


class DeleteableMeSerializer(serializers.ModelSerializer):
    """Test the 'api.api_utils.SoftDeleteViewSet' helper."""

    class Meta:
        model = DeleteableMe
        fields = "name"


class ExampleModelTagSerializer(ModelTagSerializer):
    class Meta:
        model = ExampleModelTag
        fields = ["name", "tags"]
