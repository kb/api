# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import TemplateView

from base.view_utils import BaseMixin


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"


class DashView(LoginRequiredMixin, BaseMixin, TemplateView):
    template_name = "example/dash.html"


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):
    template_name = "example/settings.html"
