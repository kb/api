# -*- encoding: utf-8 -*-
import factory

from example_api.models import DeleteableMe


class DeleteableMeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DeleteableMe

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)
