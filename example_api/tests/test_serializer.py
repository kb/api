# -*- encoding: utf-8 -*-
import pytest

from example_api.models import ExampleModelTag
from example_api.serializers import ExampleModelTagSerializer


@pytest.mark.django_db
def test_model_tag_serializer():
    x = ExampleModelTag(name="Patrick")
    x.save()
    x.tags.add("Green")
    x.tags.add("Blue")
    data = ExampleModelTagSerializer(instance=x).data
    assert {"name": "Patrick", "tags": ["Blue", "Green"]} == data


@pytest.mark.django_db
def test_model_tag_serializer_create():
    assert 0 == ExampleModelTag.objects.count()
    data = {"name": "Patrick", "tags": ["Green", "Blue", "Green"]}
    ExampleModelTagSerializer().create(data)
    assert 1 == ExampleModelTag.objects.count()
    x = ExampleModelTag.objects.first()
    assert ["Blue", "Green"] == [
        tag.name for tag in x.tags.all().order_by("name")
    ]


@pytest.mark.django_db
def test_model_tag_serializer_update():
    x = ExampleModelTag(name="Patrick")
    x.save()
    x.tags.add("Green")
    x.tags.add("Blue")
    data = {"name": "Sam", "tags": ["Dairy", "Beef"]}
    ExampleModelTagSerializer().update(x, data)
    data = ExampleModelTagSerializer(instance=x).data
    assert {"name": "Sam", "tags": ["Beef", "Dairy"]} == data
