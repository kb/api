# -*- encoding: utf-8 -*-
import pytest
from django.urls import reverse
from rest_framework import status

from api.tests.fixture import api_client
from example_api.models import DeleteableMe

from .factories import DeleteableMeFactory


@pytest.mark.django_db
def test_activity_log_soft_delete(api_client):
    del1 = DeleteableMeFactory()
    DeleteableMeFactory()
    DeleteableMeFactory()
    # verify presense
    assert 3 == DeleteableMe.objects.all().count()
    response = api_client.delete(
        reverse("api:delme-detail", args=[del1.pk]),
        content_type="application/json",
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    assert 1 == DeleteableMe.objects.filter(deleted=True).count()
    # verify soft deletion
    assert 2 == DeleteableMe.objects.filter(deleted=False).count()
    assert 3 == DeleteableMe.objects.all().count()
