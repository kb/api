# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from rest_framework import routers
from rest_framework.authtoken import views

from api.api import ActivityLogViewSet
from .api import DeleteableMeSoftDeleteViewSet
from .views import DashView, HomeView, SettingsView


router = routers.DefaultRouter()  # trailing_slash=False)
router.register(r"logs", ActivityLogViewSet, basename="log")
router.register(r"delmes", DeleteableMeSoftDeleteViewSet, basename="delme")


urlpatterns = [
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^api/", view=include("api.urls")),
    re_path(r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += staticfiles_urlpatterns()

if settings.ALLOW_DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns += [re_path(r"^__debug__/", include(debug_toolbar.urls))]
