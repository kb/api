# -*- encoding: utf-8 -*-
from rest_framework.authentication import (
    TokenAuthentication,
    SessionAuthentication,
)
from rest_framework.permissions import IsAuthenticated

from api.api_utils import SoftDeleteViewSet
from .models import DeleteableMe
from .serializers import DeleteableMeSerializer


class DeleteableMeSoftDeleteViewSet(SoftDeleteViewSet):
    """Test the 'api.api_utils.SoftDeleteViewSet' helper."""

    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = DeleteableMeSerializer

    def get_queryset(self):
        return DeleteableMe.objects.all()
