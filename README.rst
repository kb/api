API Helper
**********

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-api
  # or
  python3 -m venv venv-api

  source venv-api/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Release
=======

https://www.kbsoftware.co.uk/docs/
