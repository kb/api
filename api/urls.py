# -*- encoding: utf-8 -*-
from django.urls import re_path

from api.views import ActivityLogListView


urlpatterns = [
    re_path(
        r"^activity/log/$",
        view=ActivityLogListView.as_view(),
        name="api.activity.log.list",
    )
]
