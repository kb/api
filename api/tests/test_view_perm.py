# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from api.tests.factories import ActivityLogFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_contact_create(perm_check):
    ActivityLogFactory()
    ActivityLogFactory()
    ActivityLogFactory()
    perm_check.superuser(reverse("api.activity.log.list"))
