# -*- encoding: utf-8 -*-
import logging
import pytest

from http import HTTPStatus
from rest_framework.exceptions import APIException, ValidationError

from api.models import custom_exception_handler


def test_custom_exception_handler(caplog):
    response = custom_exception_handler(
        APIException("Patrick does not know..."), {"colour": "Green"}
    )
    assert HTTPStatus.INTERNAL_SERVER_ERROR == response.status_code
    assert {"errors": "Patrick does not know..."} == response.data
    assert [
        (
            "api.models",
            logging.ERROR,
            ">>> Patrick does not know... {'colour': 'Green'}",
        )
    ] == caplog.record_tuples


def test_custom_exception_handler_unknown(caplog):
    response = custom_exception_handler(
        Exception("REST framework does not handle this"), {"option": "Purple"}
    )
    assert HTTPStatus.INTERNAL_SERVER_ERROR == response.status_code
    assert {"errors": "REST framework does not handle this"} == response.data
    assert [
        (
            "api.models",
            logging.ERROR,
            ">>> REST framework does not handle this {'option': 'Purple'}",
        )
    ] == caplog.record_tuples


def test_custom_exception_handler_list_error_detail(caplog):
    response = custom_exception_handler(
        ValidationError("complete or delete"),
        # [ErrorDetail("complete or delete", code="invalid")],
        {"option": "Orange"},
    )
    assert HTTPStatus.BAD_REQUEST == response.status_code
    assert {"errors": "Complete Or Delete ('invalid')"} == response.data
    assert [
        (
            "api.models",
            logging.ERROR,
            (
                ">>> [ErrorDetail(string='complete or delete', code='invalid')] "
                "{'option': 'Orange'}"
            ),
        )
    ] == caplog.record_tuples
