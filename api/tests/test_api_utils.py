# -*- encoding: utf-8 -*-
import pytest

from datetime import datetime

from api.api_utils import bool_or_default, date_or_none, int_or_none
from api.models import ApiError


class MockRequest:
    def __init__(self, data):
        self.data = data

    @property
    def GET(self):
        return self.data


@pytest.mark.parametrize(
    "value,expect,default",
    [
        ("  ", False, False),
        ("", False, False),
        ("false", False, False),
        ("False", False, False),
        ("true", True, False),
        ("True", True, False),
        (0, False, False),
        (1, True, False),
        (255, True, False),
        (False, False, False),
        (True, True, False),
    ],
)
def test_bool_or_default(value, expect, default):
    mock_request = MockRequest({"extra": value})
    assert expect == bool_or_default(mock_request, "extra", default)


@pytest.mark.parametrize(
    "value,expect",
    [
        ("2021-01-31", datetime(2021, 1, 31)),
    ],
)
def test_date_or_none(value, expect):
    mock_request = MockRequest({"extra": value})
    assert expect == date_or_none(mock_request, "extra")


def test_int_or_none():
    mock_request = MockRequest({"contact": 23})
    assert 23 == int_or_none(mock_request, "contact", "ContactViewSet")


def test_int_or_none_empty_string():
    mock_request = MockRequest({"contact": ""})
    assert int_or_none(mock_request, "contact", "ContactViewSet") is None


def test_int_or_none_invalid():
    mock_request = MockRequest({"contact": "A1B2"})
    with pytest.raises(ApiError) as e:
        int_or_none(mock_request, "contact", "ContactViewSet")
    assert "'ContactViewSet' cannot get 'contact', 'A1B2'" in str(e.value)


def test_int_or_none_spaces():
    mock_request = MockRequest({"contact": "  "})
    assert int_or_none(mock_request, "contact", "ContactViewSet") is None


def test_int_or_none_string():
    mock_request = MockRequest({"contact": "23"})
    assert 23 == int_or_none(mock_request, "contact", "ContactViewSet")


def test_int_or_none_with_none():
    mock_request = MockRequest({"contact": None})
    assert int_or_none(mock_request, "contact", "ContactViewSet") is None
