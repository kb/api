# -*- encoding: utf-8 -*-
import pytest

from api.models import ActivityLog
from api.tests.factories import ActivityLogFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_ordering():
    ActivityLogFactory(user=UserFactory(username="a"), message="a")
    ActivityLogFactory(user=UserFactory(username="b"), message="b")
    ActivityLogFactory(user=UserFactory(username="c"), message="c")
    assert ["c", "b", "a"] == [x.message for x in ActivityLog.objects.all()]


@pytest.mark.django_db
def test_str():
    app = ActivityLogFactory(
        user=UserFactory(username="peter"), message="Apple"
    )
    assert "peter DEBUG Apple" in str(app)
