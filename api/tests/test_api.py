# -*- encoding: utf-8 -*-
import json
import pytest

from django.urls import reverse
from rest_framework import status

from api.models import ActivityLog
from api.tests.fixture import api_client, api_client_auth
from login.tests.factories import UserFactory
from .factories import ActivityLogFactory


@pytest.mark.django_db
def test_activity_log(api_client):
    data = {"level": "DEBUG", "message": "Fruit", "trace": "Orange"}
    assert 0 == ActivityLog.objects.count()
    response = api_client.post(
        reverse("api:log-list"),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = ActivityLog.objects.all()
    assert 1 == qs.count()
    log = qs.first()
    assert "DEBUG" == log.level
    assert "Fruit" == log.message
    assert "Orange" == log.trace
    assert "web" == log.user.username


@pytest.mark.django_db
def test_activity_log_api_client_auth(api_client_auth):
    user = UserFactory(username="auth_user_1")
    data = {"level": "DEBUG", "message": "Fruit", "trace": "Orange"}
    assert 0 == ActivityLog.objects.count()
    response = api_client_auth(user).post(
        reverse("api:log-list"),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    qs = ActivityLog.objects.all()
    assert 1 == qs.count()
    log = qs.first()
    assert "DEBUG" == log.level
    assert "Fruit" == log.message
    assert "Orange" == log.trace
    assert "auth_user_1" == log.user.username
