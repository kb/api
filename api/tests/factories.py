# -*- encoding: utf-8 -*-
import factory

from api.models import ActivityLog
from login.tests.factories import UserFactory


class ActivityLogFactory(factory.django.DjangoModelFactory):
    level = "DEBUG"

    class Meta:
        model = ActivityLog

    @factory.sequence
    def message(n):
        return "message_{:02d}".format(n)

    @factory.sequence
    def trace(n):
        return "trace_{:02d}".format(n)

    user = factory.SubFactory(UserFactory)
