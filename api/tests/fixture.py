# -*- encoding: utf-8 -*-
import pytest

from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from login.tests.factories import UserFactory


@pytest.fixture
def api_client():
    """Create a standard user, and login using a token."""
    user = UserFactory(username="web")
    token = Token.objects.create(user=user)
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION="Token {}".format(token.key))
    yield client


@pytest.fixture
def api_client_auth():
    """Client access through a user."""

    def make_token_client(user):
        token = None
        try:
            token = Token.objects.get(user=user)
        except Token.DoesNotExist:
            token = Token.objects.create(user=user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION="Token {}".format(token.key))
        return client

    yield make_token_client


def date_to_ember_iso(d):
    """To help testing REST APIs."""
    return d.isoformat().replace("+00:00", "Z")
