# -*- encoding: utf-8 -*-
from rest_framework import viewsets
from rest_framework.authentication import (
    TokenAuthentication,
    SessionAuthentication,
)
from rest_framework.permissions import IsAuthenticated

from .models import ActivityLog
from .serializers import ActivityLogSerializer


class ActivityLogViewSet(viewsets.ModelViewSet):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    http_method_names = ["post"]
    permission_classes = (IsAuthenticated,)

    queryset = ActivityLog.objects
    serializer_class = ActivityLogSerializer
