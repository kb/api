# -*- encoding: utf-8 -*-
from django.db import transaction
from rest_framework_json_api import serializers
from taggit.models import Tag

from api.models import ActivityLog


class ActivityLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityLog
        fields = ("level", "message", "trace")

    def create(self, validated_data):
        with transaction.atomic():
            instance = super().create(validated_data)
            instance.user = self.context["request"].user
            instance.save()
        return instance


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ("id", "slug", "name")


class TagListSerializer(serializers.Field):
    def to_internal_value(self, data):
        if type(data) is not list:
            raise ParseError("expected a list of data")
        return data

    def to_representation(self, obj):
        if type(obj) is not list:
            return [tag.name for tag in obj.all().order_by("name")]
        return obj


class ModelTagSerializer(serializers.ModelSerializer):
    """Base class for a model with tags."""

    tags = TagListSerializer(required=False)

    def create(self, validated_data):
        tags = validated_data.pop("tags")
        with transaction.atomic():
            instance = super().create(validated_data)
            for x in tags:
                instance.tags.add(x)
        return instance

    def update(self, instance, validated_data):
        tags = validated_data.pop("tags", [])
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.tags.clear()
            for x in tags:
                instance.tags.add(x)
        return instance
