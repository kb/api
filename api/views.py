# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.views.generic import ListView

from api.models import ActivityLog


class ActivityLogListView(LoginRequiredMixin, SuperuserRequiredMixin, ListView):
    paginate_by = 20

    model = ActivityLog
    template_name = "api/activity_group_list.html"
