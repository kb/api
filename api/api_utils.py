# -*- encoding: utf-8 -*-
import collections

from dateutil import parser
from distutils.util import strtobool
from django.core.paginator import Paginator
from rest_framework import viewsets
from rest_framework.pagination import BasePagination
from rest_framework.response import Response
from rest_framework_json_api import views

from .models import ApiError


def bool_or_default(request, variable_name, default):
    """Use in a ``ViewSet`` to get a boolean value.

    .. tip:: Code copied from ``activiti_bool`` (``workflow/activiti.py``).

    .. tip:: Was called ``bool_or_false``.  To update use,
             ``bool_or_default(self.request, "extra", False)``

    ::

      from api.api_utils import bool_or_default

      def get_queryset(self):
          extra = bool_or_default(self.request, "extra", False)

    """
    value = request.GET.get(variable_name) or None
    if type(value) is bool:
        result = value
    elif type(value) is int:
        result = bool(value)
    elif value is None:
        result = None
    elif str(value).strip() == "":
        result = None
    else:
        result = bool(strtobool(value))
    if result is None:
        return default
    else:
        return result


def date_or_none(request, variable_name):
    """Use in a ``ViewSet`` to get a 'datetime' value.

    .. tip:: Code copied from ``activiti_date`` (``workflow/activiti.py``).

    ::

      from datetime import datetime, time
      from api.api_utils import date_or_none

      def get_queryset(self):
          from_date = date_or_none(self.request, "from")
          if from_date:
              # set the time to the start of the day::
              from_date = datetime.combine(from_date.date(), time.min)
          to_date = date_or_none(self.request, "to")
          if to_date:
              # set the time to the end of the day::
              to_date = datetime.combine(to_date.date(), time.max)

    """
    result = None
    date_as_str = request.GET.get(variable_name, None)
    if date_as_str:
        result = parser.parse(date_as_str)
    return result


def int_or_none(request, variable_name, class_name):
    """Use in a ``ViewSet`` to get an integer value.

    .. tip:: The ``class_name`` is only used to throw a helpful exception
             message.

    ::

      from api.api_utils import int_or_none

      def get_queryset(self):
          contact_pk = int_or_none(
              self.request, "contact", self.__class__.__name__
          )

    """
    result = request.GET.get(variable_name) or None
    try:
        result = result.strip() or None
    except AttributeError:
        pass
    if result:
        try:
            result = int(result)
        except ValueError:
            raise ApiError(
                "'{}' cannot get '{}', '{}'".format(
                    class_name, variable_name, result
                )
            )
    return result


# def ordereddict_to_json(ordered_dict):
#    Moved to 'scrutiny/models.py' in the 'training' app...


class KbRestPagination(BasePagination):
    """Pagination for our Ember projects.

    To use in our viewset classes::

      from api.api_utils import KbRestPagination, SoftDeleteViewSet

      class IssueViewSet(SoftDeleteViewSet):
          authentication_classes = (TokenAuthentication,)
          pagination_class = KbRestPagination
          permission_classes = (IsAuthenticated,)
          serializer_class = IssueSerializer

    1. Our ``SoftDeleteViewSet`` class is derived from ``ModelViewSet``
       (``rest_framework/viewsets.py``)
    2. The ``ModelViewSet`` class is derived from ``ListModelMixin``
       (``rest_framework/mixins.py``)
    3. The ``ListModelMixin`` has a ``list`` method which uses the
       ``pagination_class`` which for our viewsets is ``KbRestPagination``.

    The original code for ``KbRestPagination`` was copied from
    ``PageNumberPagination`` (``rest_framework/pagination.py``).

    """

    def get_paginated_response(self, data):
        return Response(
            collections.OrderedDict(
                [
                    ("count", self.page.paginator.num_pages),
                    (
                        "next",
                        (
                            self.page.next_page_number()
                            if self.page.has_next()
                            else None
                        ),
                    ),
                    (
                        "previous",
                        (
                            self.page.previous_page_number()
                            if self.page.has_previous()
                            else None
                        ),
                    ),
                    ("results", data),
                    ("total", self.page.paginator.count),
                ]
            )
        )

    def paginate_queryset(self, queryset, request, view=None):
        try:
            page_number = int(request.GET.get("page", None))
        except TypeError:
            page_number = 1
        try:
            size = int(request.GET.get("per_page", None))
        except TypeError:
            # match 'perPage' from the route
            size = 20
        paginator = Paginator(queryset, size)
        self.page = paginator.page(page_number)
        return list(self.page)


class SoftDeleteViewSet(views.ModelViewSet):
    """An API view which marks records as deleted, rather than deleting them.

    .. warning ``views.ModelViewSet`` is from ``rest_framework_json_api``.
               If your views are using the REST API, then use
               ``SoftDeleteViewSetRestApi`` (see below).

    """

    def perform_destroy(self, instance):
        instance.set_deleted(self.request.user)


class SoftDeleteViewSetRestApi(viewsets.ModelViewSet):
    """An API view which marks records as deleted, rather than deleting them.

    .. warning ``viewsets.ModelViewSet`` is from ``rest_framework``.
               If your views are using the JSON API, then use
               ``SoftDeleteViewSet`` (see above).

    """

    def perform_destroy(self, instance):
        instance.set_deleted(self.request.user)
