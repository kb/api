# -*- encoding: utf-8 -*-
import logging

from django.contrib.auth.models import User
from django.db import models
from http import HTTPStatus
from rest_framework.exceptions import (
    _get_error_details,
    APIException,
    ErrorDetail,
)
from rest_framework.response import Response
from rest_framework.views import exception_handler

from base.model_utils import TimeStampedModel


logger = logging.getLogger(__name__)


class ApiError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class JsonApiValidationError(APIException):
    """Copied from 'rest_framework/exceptions'."""

    status_code = HTTPStatus.UNPROCESSABLE_ENTITY
    default_detail = "Invalid input."
    default_code = "invalid"

    def __init__(self, detail=None, code=None):
        if detail is None:
            detail = self.default_detail
        if code is None:
            code = self.default_code

        # For validation failures, we may collect many errors together,
        # so the details should always be coerced to a list if not already.
        if isinstance(detail, tuple):
            detail = list(detail)
        elif not isinstance(detail, dict) and not isinstance(detail, list):
            detail = [detail]

        self.detail = _get_error_details(detail, code)


class ActivityLogManager(models.Manager):
    def create_activity_log(self, user, level, message):
        x = self.model(user=user, level=level, message=message)
        x.save()
        return x


class ActivityLog(TimeStampedModel):
    ERROR = "Error"
    INFO = "Info"

    user = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.CASCADE, related_name="+"
    )
    level = models.CharField(max_length=30, help_text="Log level e.g. 'Error'")
    message = models.TextField()
    trace = models.TextField(blank=True, null=True)
    objects = ActivityLogManager()

    class Meta:
        ordering = ["-pk"]
        verbose_name = "Activity Log"
        verbose_name_plural = "Activity Logs"

    def created_as_str(self):
        return self.created.strftime("%d/%m/%Y %H:%M:%S")

    def __str__(self):
        username = self.user.username if self.user else "[no-user]"
        return "{}. {} {} {} {}".format(
            self.pk, self.created_as_str(), username, self.level, self.message
        )


def custom_exception_handler(exc, context):
    """Custom exception handler for Ember.

    Custom exception handling
    https://www.django-rest-framework.org/api-guide/exceptions/#custom-exception-handling

    .. tip:: For this exception handler to work nicely ``raise`` an exception
             (do not return an error response) e.g. ``raise PermissionDenied``
             rather than ``return HttpResponseForbidden``.

    To configure::

      REST_FRAMEWORK = {"EXCEPTION_HANDLER": "api.models.custom_exception_handler"}

    """
    logger.error(">>> {} {}".format(exc, context), exc_info=True)
    response = exception_handler(exc, context)
    if response is None:
        response = Response(
            {"errors": str(exc)}, status=HTTPStatus.INTERNAL_SERVER_ERROR
        )
    else:
        detail = None
        if type(response.data) is list:
            if response.data:
                x = response.data[0]
                if type(x) is ErrorDetail:
                    detail = "{} ('{}')".format(x.title(), x.code)
                    response.data = {"errors": detail}
        elif type(response.data) is dict:
            detail = response.data.pop("detail", None)
            if detail:
                response.data["errors"] = str(detail)
    return response
